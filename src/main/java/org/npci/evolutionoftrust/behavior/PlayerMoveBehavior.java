package org.npci.evolutionoftrust.behavior;

import org.npci.evolutionoftrust.game.MoveType;

public interface PlayerMoveBehavior {

    final PlayerMoveBehavior CHEAT_BEHAVIOR = () -> MoveType.CHEAT;
    final PlayerMoveBehavior COOPERATE_BEHAVIOR = () -> MoveType.CHEAT;

    MoveType getMove();

}
