package org.npci.evolutionoftrust.behavior;



/*
Small

- Smallest test case
- Smallest implementation
- Class - abstract class - implementation inheritance
- abtract class - interface - interface inheritance
- Class - object (stateless)

Duplication is hard to spot, small is hard to spot..

 */

import org.npci.evolutionoftrust.game.MoveType;

public class AlternateBehaviour implements PlayerMoveBehavior {

    private MoveType moveType;

    public AlternateBehaviour() {
        this.moveType = new CooporateBehavior().getMove();
    }

    @Override
    public MoveType getMove() {
        if(this.moveType == new CooporateBehavior().getMove()) {
            moveType =  new CheatBehavior().getMove();
        } else {
            moveType = new CooporateBehavior().getMove();
        }
        return moveType;
    }
}
