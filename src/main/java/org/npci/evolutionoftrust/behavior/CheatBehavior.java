package org.npci.evolutionoftrust.behavior;

import org.npci.evolutionoftrust.game.MoveType;

public class CheatBehavior implements PlayerMoveBehavior {
    @Override
    public MoveType getMove() {
        return MoveType.CHEAT;
    }
}
