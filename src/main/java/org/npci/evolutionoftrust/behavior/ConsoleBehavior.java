package org.npci.evolutionoftrust.behavior;

import org.npci.evolutionoftrust.game.MoveType;

import java.util.Scanner;

public class ConsoleBehavior implements PlayerMoveBehavior {
    Scanner scannerInput;
    public ConsoleBehavior(Scanner scannerInput) {
        this.scannerInput = scannerInput;
    }

    @Override
    public MoveType getMove() {
       String code = scannerInput.nextLine();
       if(code.equalsIgnoreCase("co")) {
           return MoveType.CORPORATE;
        } else {
           return MoveType.CHEAT;
        }
    }
}
