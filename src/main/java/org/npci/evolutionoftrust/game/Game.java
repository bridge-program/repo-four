package org.npci.evolutionoftrust.game;

import org.npci.evolutionoftrust.observer.GameObserver;
import org.npci.evolutionoftrust.observer.ScoreBoard;
import org.npci.evolutionoftrust.player.Player;
import org.npci.evolutionoftrust.ruleengine.RuleEngine;

import java.util.Arrays;
import java.util.List;

public class Game {
    private final Player one;
    private final Player two;
    private final int numberOfRounds;
    private final List<GameObserver> gameObservers;
    

    public Game(Player one, Player two, int numberOfRounds,GameObserver... gameObservers) {
        this.one = one;
        this.two = two;
        this.numberOfRounds = numberOfRounds;
        this.gameObservers = Arrays.asList(gameObservers);
    }

    public Score score() {
        RuleEngine ruleEngine = new RuleEngine(one.getMove(), two.getMove());
        Score score = ruleEngine.score();
        for(int i = 0; i < numberOfRounds; i++) {
            one.setScoreFor(i+1, score.getForPlayerOne());
            two.setScoreFor(i+1, score.getForPlayerTwo());
        }
        gameObservers.forEach(GameObserver::gameEnded);
        return score.times(numberOfRounds);
    }

}
