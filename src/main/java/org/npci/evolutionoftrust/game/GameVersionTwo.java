package org.npci.evolutionoftrust.game;

import org.npci.evolutionoftrust.behavior.ConsoleBehavior;
import org.npci.evolutionoftrust.ruleengine.RuleEngine;
import org.npci.evolutionoftrust.player.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//TODO design the scoring as a behavior
public class GameVersionTwo extends Game {

    private Player one;
    private Player two;
    private int numberOfRounds;

    public GameVersionTwo(Player one, Player two, int numberOfRounds) {
        super(one, two, numberOfRounds);
        this.one = one;
        this.two = two;
        this.numberOfRounds = numberOfRounds;

    }

    @Override
    public Score score() {
        Map<Player, Integer> totalScores = new HashMap<>();
        totalScores.put(one, 0);
        totalScores.put(two, 0);
        RuleEngine ruleEngine = null;
        Score score = null;
        for (int i = 0; i < numberOfRounds; i++) {
            ruleEngine = new RuleEngine(one.getMove(), two.getMove());
            score = ruleEngine.score();
            one.setScoreFor(i + 1, score.getForPlayerOne());
            two.setScoreFor(i + 1, score.getForPlayerTwo());
            totalScores.put(one, totalScores.get(one) + one.scoreAt(i + 1));
            totalScores.put(two, totalScores.get(two) + two.scoreAt(i + 1));
        }


        return new Score(totalScores.get(one), totalScores.get(two));
    }


    public static void main(String args[]) {
        Scanner player1Scan = new Scanner(System.in);
        Scanner player2Scan = new Scanner(System.in);

        Player player1 = new Player("test", new ConsoleBehavior(player1Scan));
        Player player2 = new Player("test", new ConsoleBehavior(player2Scan));

        Score score = new GameVersionTwo(player1,player1,2).score();


    }

}