package org.npci.evolutionoftrust.observer;

public interface GameObserver {
       void gameEnded();
}
