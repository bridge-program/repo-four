package org.npci.evolutionoftrust.player;

import org.npci.evolutionoftrust.behavior.AlternateBehaviour;
import org.npci.evolutionoftrust.behavior.CheatBehavior;
import org.npci.evolutionoftrust.behavior.CooporateBehavior;
import org.npci.evolutionoftrust.exception.NoSuchPlayerException;

public class PlayerFactory {

    public static Player getPlayer(String playerType, String name) {

        switch(playerType) {
            case "COOPERATE":
                return new Player(name,new CooporateBehavior());
            case "CHEAT":
                return new Player(name, new CheatBehavior());
            case "ALTERNATE":
                return new Player(name, new AlternateBehaviour());
            default:
                throw new NoSuchPlayerException();
        }

    }
}
