package org.npci.evolutionoftrust.player;

import org.npci.evolutionoftrust.game.MoveType;
import org.npci.evolutionoftrust.behavior.PlayerMoveBehavior;
import org.npci.evolutionoftrust.exception.RoundNotPlayedYetException;

import java.util.HashMap;
import java.util.Map;

public class Player {

    private final String name;
    private final PlayerMoveBehavior behavior;

    public Player(String name, PlayerMoveBehavior behavior) {
        this.name = name;
        this.behavior = behavior;
    }

    private final Map<Integer, Integer> scores = new HashMap<>();

    public int scoreAt(int roundNumber) {
        if (scores.containsKey(roundNumber)) {
            return scores.get(roundNumber);
        }
        throw new RoundNotPlayedYetException();
    }

    public MoveType getMove() {
        return this.behavior.getMove();
    }

    public void setScoreFor(int roundNumber, int score) {
        this.scores.put(roundNumber, score);
    }
}
