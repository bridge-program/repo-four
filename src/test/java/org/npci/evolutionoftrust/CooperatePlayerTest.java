package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Test;
import org.npci.evolutionoftrust.behavior.AlternateBehaviour;
import org.npci.evolutionoftrust.behavior.CooporateBehavior;
import org.npci.evolutionoftrust.exception.RoundNotPlayedYetException;
import org.npci.evolutionoftrust.player.Player;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.npci.evolutionoftrust.game.MoveType.*;

class CooperatePlayerTest {

    @Test
    void playerShouldAlwaysCorporate() {
        assertThat(new Player("a",new CooporateBehavior()).getMove(), is(CORPORATE));
    }

    @Test
    void shouldGiveScoreForTheRoundThatHasBeenPlayed() {
        Player player = new Player("a", new CooporateBehavior());
        player.setScoreFor(1, 0);

        assertThat(player.scoreAt(1), is(0));
    }

    @Test
    void shouldGiveScoreForAnyRoundThatHasBeenPlayed() {
        Player player = new Player("a", new CooporateBehavior());
        player.setScoreFor(1, 0);
        player.setScoreFor(2, 1);

        assertThat(player.scoreAt(1), is(0));
        assertThat(player.scoreAt(2), is(1));
    }

    @Test
    void shouldThrowRoundNotPlayedExceptionForAScoreForARoundThatHasNotBeenPlayedYet() {
        Player player = new Player("a", new CooporateBehavior());
        assertThrows(RoundNotPlayedYetException.class, () -> player.scoreAt(1));
    }

    @Test
    void checkIfFirstMoveOfAlternatingPlayerIsAlwaysCheat() {
        Player player = new Player("Dhruv",new AlternateBehaviour());
        assertThat(player.getMove(),is(CHEAT));
        //this is the next move
        //player.setMove(CORPORATE);
        //assertThat(player.getMove(),is(CORPORATE));

    }
    @Test
    void checkIfSecondMoveOfAlternatingPlayerIsAlwaysCooperate() {
        Player player = new Player("Dhruv",new AlternateBehaviour());
        player.getMove();
        assertThat(player.getMove(),is(CORPORATE));
        //this is the next move
        //player.setMove(CORPORATE);
        //assertThat(player.getMove(),is(CORPORATE));

    }

    @Test
    void checkIfThirdMoveOfAlternatingPlayerIsAlwaysCheat() {
        Player player = new Player("Dhruv",new AlternateBehaviour());
        player.getMove();
        player.getMove();
        assertThat(player.getMove(),is(CHEAT));
        //this is the next move
        //player.setMove(CORPORATE);
        //assertThat(player.getMove(),is(CORPORATE));

    }

   /* @Test
    void checkIfTheIsCreatedWhenTheMoveTypeConsoleInputIsCooporate() {
        String playerType = "COOPERATE";
        CooperatePlayer player = ConsolePlayer.getPlayer(playerType,"Dinakaran");
        assertEquals(player.getMove(),CORPORATE);
    }

    @Test
    void checkIfTheCheatPlayerIsCreatedWhenTheMoveTypeConsoleInputIsCheat() {
        String playerType = "CHEAT";
        CooperatePlayer player = ConsolePlayer.getPlayer(playerType,"Dinakaran");
        assertEquals(player.getMove(),CHEAT);
    }

    @Test
    void checkIfTheExceptionThownForInvalidPlayer() {
        String playerType = "ALTERX";
        assertThrows(NoSuchPlayerException.class,()->{ ConsolePlayer.getPlayer(playerType,"Dinakaran");});

    }*/

}