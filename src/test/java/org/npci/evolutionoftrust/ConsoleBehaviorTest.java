package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Test;
import org.npci.evolutionoftrust.behavior.ConsoleBehavior;
import org.npci.evolutionoftrust.player.Player;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.npci.evolutionoftrust.game.MoveType.CHEAT;
import static org.npci.evolutionoftrust.game.MoveType.CORPORATE;

public class ConsoleBehaviorTest {
    @Test
    public void checkIfConsoleBehaviorReturnsCooperatePlayer() {
        Player player = new Player("test",new ConsoleBehavior(new Scanner("co")));
        assertEquals(player.getMove(),CORPORATE);
    }

    @Test
    public void checkIfConsoleBehaviorReturnsCheatPlayer() {
        Player player = new Player("test",new ConsoleBehavior(new Scanner("ch")));
        assertEquals(player.getMove(),CHEAT);
    }

}
