package org.npci.evolutionoftrust;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.npci.evolutionoftrust.behavior.AlternateBehaviour;
import org.npci.evolutionoftrust.behavior.ConsoleBehavior;
import org.npci.evolutionoftrust.behavior.CooporateBehavior;
import org.npci.evolutionoftrust.game.Game;
import org.npci.evolutionoftrust.game.GameVersionTwo;
import org.npci.evolutionoftrust.game.Score;
import org.npci.evolutionoftrust.observer.ScoreBoard;
import org.npci.evolutionoftrust.player.Player;

import java.util.Scanner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    void shouldBeAbleToPlayOneRound() {
        Player one = new Player("one",new CooporateBehavior());
        Player two = new Player("two",new CooporateBehavior());
        Score actualScore = new Game(one, two, 1).score();

        assertThat(actualScore, is(new Score(2,2))); // TODO - One assertion per test?
    }

    @Test
    void shouldBeAbleToPlayTwoRound() {
        Player one = new Player("one",new CooporateBehavior());
        Player two = new Player("two",new CooporateBehavior());
        Score actualScore = new Game(one, two, 2).score();

        assertThat(actualScore, is(new Score(4,4))); // TODO - One assertion per test?
    }

    @Test
    public void shouldBeAbleToPlayGameBetweenTwoPlayersWhoAlternateFor1Round() {
        Player dhruv = new Player("Dhruv",new AlternateBehaviour());
        Player maha = new Player("Maha",new AlternateBehaviour());

        Score score = new Game(dhruv, maha, 1).score();
        assertThat(score, is(new Score(-1,-1)));
    }

    @Test
    public void shouldBeAbleToPlayGameBetweenTwoPlayersWhoAlternateFor2Round() {
        Player dhruv = new Player("Dhruv",new AlternateBehaviour());
        Player maha = new Player("Maha",new AlternateBehaviour());
        Score score = new GameVersionTwo(dhruv, maha, 2).score();
        assertThat(score, is(new Score(-1 + 2,-1 + 2)));
    }

    @Test
    public void shouldBeAbleToPlayGameBetweenTwoPlayersPlayerOneCorporatePlayerTwoAlternate() {
        Player dhruv = new Player("Dhruv",new CooporateBehavior());
        Player maha = new Player("Maha",new AlternateBehaviour());
        Score score = new GameVersionTwo(dhruv, maha, 2).score();
        assertThat(score, is(new Score(-1+2,3+2)));
    }

    @Test
    public void checkTotalScoreWherePlayerOneConsoleCooperatePlayerPlayerTwoConsoleCooperatePlayer() {
        Player dinakaran = new Player("test",new ConsoleBehavior(new Scanner("co")));
        Player balaji = new Player("test",new ConsoleBehavior(new Scanner("co")));
        Score score = new GameVersionTwo(dinakaran, balaji, 1).score();
        assertThat(score, is(new Score(2,2)));
    }

    public void checkTotalScoreWherePlayerOneCooperatePlayerPlayerTwoConsoleCheatPlayer() {
        Player dinakaran = new Player("test",new CooporateBehavior());
        Player balaji = new Player("test",new ConsoleBehavior(new Scanner("ch")));
        Score score = new GameVersionTwo(dinakaran, balaji, 1).score();
        assertThat(score, is(new Score(-1,3)));

    }

}
